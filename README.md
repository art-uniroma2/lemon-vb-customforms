Lemon VocBench Custom Forms
===========================

This repository contains an experimental implementation of the [Lemon Design Patterns](https://github.com/jmccrae/lemon.patterns) as [VocBench](http://vocbench.uniroma2.it/) Custom Forms.

While the patterns were originally based on [Monnet Lemon](http://lemon-model.net/), they were subsequently 
updated to the [OntoLex Lemon](https://www.w3.org/2016/05/ontolex/) specification, developed by the
[Ontology-Lexicon W3C Community Group](https://www.w3.org/2016/05/ontolex/).

Custom forms, in general, are a mechanism inside VocBench for providing extending support to the creation and
visualization of resources arranged into complex (still predictable) graph patterns. The forms provided inside
this repository exploit that mechanism to ease the development of an _ontology lexicon_
represented in RDF via the OntoLex Lemon model.

Requirements
------------
These forms are compatible with VocBench 3 (version >= 8.0)

Installation
------------
The data model underlying the custom forms mechanism distinguishes between:
* forms (the actual form definition);
* form collections (a cohesive group of forms);
* and custom form configurations (binding ontology properties and classes to form collections).

The folder `customForms` contains the files above, organized in different subfolders

To install these files, it is necessary to edit some files in the backing [Semantic Turkey](http://semanticturkey.uniroma2.it/)
server, when it is off.

To install the files at system level copy them in the appropriate folder inside `SemanticTurkeyData`
reflecting their position in the repository:

	SemanticTurkeyData
	 |
	 +-- system
	      |
	      +-- customForms
	           |
	           +- formCollections
	           |
	           +- forms

System-level installation will affect every project managed by the server.

For a project-level installation, it is necessary to copy the files in an analogous folder structure inside
the directory containing the data of the project:

	SemanticTurkeyData
	 |
	 +-- projects
	      |
	      +-- {project X}
		       |
		       +-- customForms
		            |
		            +- formCollections
		            |
		            +- forms
		           
The two approaches are not mutually exclusive, rather they are complementary, because you could desire to
install different elements at different levels. An effective strategy consists in installing forms and 
collections at the system-level, while the _custom form configuration_ is done at the level of individual
projects (so that projects are not required to use the new entries).

A note of precaution. The file describing a _form_ or a _form collection_ can be copied safely
to the target destination, assuming that no homonym element was installed previously.
Conversely, the _custom form configuration_ should not be copied as it is, otherwise it would
overwrite any existing configuration. Instead, its content should be merged in the file already
existing inside the target destination.